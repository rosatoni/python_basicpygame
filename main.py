import sys
import pygame

def run_app():
    # Initialize pygame
    pygame.init()

    # Get a screen surface window
    screen = pygame.display.set_mode((1200, 800))

    # Create Pygame clock object.
    clock = pygame.time.Clock()

    # Set the window title
    pygame.display.set_caption("Basic Example")

    screen.fill((0, 0, 255))
    pygame.display.flip()

    # Start the main loop for the app.
    while True:
        milliseconds = clock.tick(30) # .tick() gets the time (the milliseconds) that have passed since the previous call... and halts the execution if needed to reach the passed in FPS

        # Watch for keyboard and mouse events.
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        # Paint blueinto in the surface window
        screen.fill( (0, 255, 255)  )

        # Make the most recently drawn screen visible.
        #pygame.display.flip()

#---------------------------------------------------------

run_app()
